const express = require ('express');
const { buscarUfs, buscarUfPorId, buscarUfsPorNome } = require('./servicos');

const app = express();

app.get('/ufs', (req, res) => {
    const nomeuf = req.query.busca;
    const resultado = nomeuf ? buscarUfsPorNome(nomeuf): buscarUfs();

    if (resultado.length > 0){
        res.json(resultado);
    } else {
        res.status(400).send({"erro":"nenhuma UF encontrada"});
    }
});

app.get('/ufs/:iduf', (req, res) => {
   const uf = buscarUfPorId(req.params.iduf);
   
   if (uf) {
    res.json(uf);
  } else if (isNaN(parseInt(req.params.iduf))) {
    res.status(400).send({ "erro": "Requisição inválida" });
  } else {
    res.status(404).send({ "erro": "UF não encontrada" });
  }
})

app.listen(8080, () => {
    console.log('Servidor node iniciado');
})