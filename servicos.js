const colecaoUf = require('./dados');

const buscarUfs = () => {
    return colecaoUf;
  }
  
  const buscarUfsPorNome = (value) => {
    return colecaoUf.filter(uf => uf.nome.toLowerCase().includes(value.toLowerCase()));
  };
  
  const buscarUfPorId = (value) => {
    const idUF = parseInt(value);
    return colecaoUf.find(uf => uf.id === idUF);
  }

  module.exports = {
    buscarUfs,
    buscarUfsPorNome,
    buscarUfPorId,
};